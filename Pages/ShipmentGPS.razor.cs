using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Timers;
using System.Threading.Tasks;

using MatBlazor;
using FIOWeb.Models;

namespace FIOWeb.Pages
{
    public partial class ShipmentGPS
    {
        private List<ShipmentGPSModel> Shipments = null;

        protected override async Task OnInitializedAsync()
        {
            GlobalAppState.OnChange += StateHasChanged;

            var shipmentsRequest = new Web.Request(HttpMethod.Get, "/contract/shipments", await GlobalAppState.GetAuthToken());
            Shipments = await shipmentsRequest.GetResponseAsync<List<ShipmentGPSModel>>();
        }

        public void Dispose()
        {
            GlobalAppState.OnChange -= StateHasChanged;
        }
    }
}